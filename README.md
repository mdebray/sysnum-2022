# Projet de sysnum de Abel Douzal, Rémy Kimbrough et Maurice Debray

https://git.eleves.ens.fr/mdebray/sysnum-2022/

## Compiler le projet

Avec nix:

```
nix-build -A clock_sim[_fast] # construit un script shell qui execute le simulateur sur le programme
```

Sans nix:

```
# Eventuellement mettre un venv python puis installer les dépendances:
make setup-env
# Compiler:
make all
```
## Executer les programmes d'horloge

Avec nix:
```
python input_generator.py -n - | ./result
```

Sans nix:
```
python input_generator.py -n - | ./betterave assembler/clock[_fast].hex
```

Pour ne pas utiliser la date actuelle, il faut utiliser un fichier intermédiaire:
```
python input_generator.py infile
./bettrave assembleur/clock[_fast].hex < infile
```

## Changer la vitesse

Il suffit de changer la valeur donnée dans la directive `.set` au début du fichier `clock.s`.

## Structure

Le projet contient
- des fichiers décrivant une netlist d'un microprocesseur suivant une
  architecture RISC-V quasi complète (dossier cpu/)
- un compilateur de netlist vers C ainsi qu'un simulateur utilisant le fichier
  .c produit
- un programme d'horloge en assembleur (assembler/clock.s) ainsi qu'une version
  sans limite de vitesse qui tourne aussi vite que le permet notre simulateur
- de quoi compiler le tout


## Instruction set

En vue de faciliter le décodage d'instructions, on a implémenté une partie de
l'architecture RISC-V de base, c'est à dire les instructions suivantes :
- LUI
- AUIPC 
- JAL/JALR 
- BEQ
- BNE
- BLT/BLTU
- BGE/BGEU
- LB/LH/LW
- LBU/LHU
- SB/SH/SW
- ADDI 
- SLTI/SLTIU 
- XORI/ORI/ANDI
- SLLI/SRLI
- ADD
- SLT/SLTU 
- XOR/OR/AND
- SLL/SRL


## Registres
On a 33 registres de taille 32 bits : `x0` à `x31` et le registre `pc`.
- Le registre `x0` est toujours égal à 0.
- Le registre `pc` contient l'adresse de l'instruction en cours d'exécution.
- La taille des registres de 32 bits donne une marge pour le nombre de tics par
  seconde de 3GHz, ce qui est (très) confortable. 

## Mémoire

Toutes les adresses doivent être alignées sur leur taille (addresses de 32 bit
alignées sur 32 bits par exemple).

### Plan d'adressage

Les addresses `0x0000_0000` à `0x0400_0000` (exclu) sont la ROM, `0x8000_0000`
à `0x8400_0000` sont la RAM, et de `0xc000_0000` à `0xffff_ffff` correspond à
une écriture/lecture sur le bus d'IO.

seul `SW` est supporté par les sorties du bus d'IO (il est simple d'améliorer
ça mais on a pas eu le temps)

#### Input
Le bus d'IO est composé d'une entrée sur 32 bit. Le lecture à n'importe quelle
addresse de la plage d'IO renvoie la valeur de ce bus d'entrée.

#### Output
En sortie le bus d'IO est composé d'une addresse sur 28 bits (comme on ne peut
que écrire sur 32 bits alignés on enlève les deux bits de poids faible de
l'addresse côté RISC-V et on enlève les deux bits de poids forts car constants
sur la plage d'addresse des IO)

La donnée est un bus de 32 bits.

Notre simulateur écoute sur ce bus aux adresses `0x1` à `0x8` (exclu) pour
créer son affichage 2 segment. Chaque adresse correspond à deux chiffres codés
chacun sur 1 octet (`0x1` est donc les nombre de secondes, `0x2` les minutes,
...).

Les bits servant à allumer les segments sont les suivants:
```
   8         0
  ----      ----
d|    |9  5|    |1
 | e  |    | 6  |
  ----      ----
 |c   |a   |4   |2
 |    |    |    |
  ----      ----
    b         3
```

## Simulateur de netlist
Le simulateur de netlist utilisé est un compilateur qui transforme la netlist
en code C, qui est lui-même compilé en binaire. Cela fournit un simulateur plus
rapide que d'utiliser un interpréteur.


## Horloge

### Fonctionnement général

Le programme commence par lire la date fournie en entrée, charge dans la RAM le
nombre de jours de chaque mois (utile car on est mené à changer le nombre de
jours de février pour les années bissextiles) et charge les limites pour chacun
des compteurs utilisés : ticks, secondes, minutes, heures, mois, années modulo
4, 100 et 400, ainsi qu'un compteur de siècles.

Après cela, les données d'affichage initiales sont chargées puis le programme
commence à exécuter la boucle principale, qui incrémente les compteurs du plus
rapide au plus lent de la manière suivante :
- on incrémente le compteur
- s'il n'a pas atteint sa limite, on revient au début (en mettant éventuellement à jour l'affichage)
- sinon, on le réinitialise et on passe au compteur suivant

En outre, la mise à jour des compteurs sur les années est accompagnée d'une
modification du nombre de jours de février.

Le programme fait un usage abondant des registres :
- `x1` à `x4` sont utilisés pour stocker les adresses importantes de la mémoire
  (entrée, jours des mois, sortie, ROM de données)
- `x10` à `x19` et `x30` sont utilisés pour les compteurs
- `x20` à `x29` sont utilisés pour les limites des compteurs
- `x5` à `x8` sont utilisés de façon plus occasionnelle pour des manipulations
  de valeurs 

### Affichage
Le programme place à des adresses prédéfinies les nombres à afficher pour
chaque compteur, sous un format décrivant un affichage sur un 7 segments. La
description en 7 segments pour chaque nombre de 0 à 99 est stocké dans un bloc
contigu de la ROM de données.

Le simulateur délègue à un thread l'affichage des données, qui l'affiche en
console, ce qui a le mérite de grandement améliorer les performances.

## Nos regrets

Ne pas avoir implémenté un vrai composant permettant de faire du real time et
de se contenter de compter des ticks (c'est assez imprécis selon les cpu et la
façon dont l'os gère le processus)
