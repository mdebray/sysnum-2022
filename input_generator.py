import argparse
import datetime
import sys

parser = argparse.ArgumentParser(description="Generateur d'entrée pour betterave")
parser.add_argument("out_file", nargs=1)
parser.add_argument("-n", "--now", action="store_true", help="Date is now")
args = parser.parse_args()

outfile = args.out_file[0]

if not args.now:
    a = int(input("Année"))
    m = int(input("Mois"))
    j = int(input("Jour"))
    h = int(input("Heure"))
    mm = int(input("Minute"))
    s = int(input("Seconde"))
else:
    d = datetime.datetime.now()
    a = d.year
    m = d.month
    j = d.day
    h = d.hour
    mm = d.minute
    s = d.second

out = "0\n" * 21

out += hex(s)[2:] + "\n"
out += hex(mm)[2:] + "\n"
out += hex(h)[2:] + "\n"
out += hex(j)[2:] + "\n"
out += hex(m)[2:] + "\n"
out += hex(a % 4)[2:] + "\n"
out += hex(a % 100)[2:] + "\n"
out += hex(a % 400)[2:] + "\n"
out += hex(a // 100)[2:]

if outfile != "-":
    with open(outfile, "w") as f:
        f.write(out)
else:
    print(out)
