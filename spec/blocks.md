# ALU

Fonctions:
 - Add
 - And
 - Or
 - Comparaison stricte
 - Xor
 - Egalite

Inputs:
 - a[32]
 - b[32]
 - op[3]

Output:
 - out[32]

# Reg

Fonctions:
 - `x0` = `/dev/zero`

Inputs:
 - Raddr1[5]
 - Raddr2[5]
 - Saddr[5]
 - Sval[32]

Outputs:
 - Rval1[32]
 - Rval2[32]

# Memory

Inputs:
 - ROMaddr[31]
 - StoreEnable[1]
 - RAMaddr[31] : On lit et on écrit au même endroit car on ne fait jamais les deux en même temps
 - RAMSdata[31]

Outputs:
 - ROMval[32]
 - RAMval[32]

Toutes les adresses doivent être alignées sur leur taille (addresses de 32 bit alignées sur 32 bits par exemple).

## Plan d'adressage

Les addresses `0x0000_0000` à `0x0400_0000` (exclu) sont la ROM, `0x8000_0000` à `0x8400_0000` sont la RAM, et de `0xc000_0000` à `0xffff_ffff` correspond à une écriture/lecture sur le bus d'IO.

seul `SW` est supporté par les sorties du bus d'IO (il est simple d'améliorer ça mais on a pas eu le temps)

# Décodeur d'instruction

Récupère l'instruction et route bien les différents signaux sur les modules pour que ça marche
