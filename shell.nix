{ pkgs ? (import ./nix/nixpkgs.nix { }) }:
let
  carottepy = import ./nix/carottepy.nix { inherit pkgs; };
  nl-sim = import ./nix/nl-sim.nix { inherit pkgs; };
in 
pkgs.pkgsCross.riscv32-embedded.mkShell {
  packages = [
    pkgs.spike
    pkgs.dtc
    pkgs.pkgsCross.riscv32-embedded.riscv-pk
    carottepy nl-sim
    pkgs.gcc
  ];
  shellHook = ''
    export PREFIX=riscv32-none-elf-
    '';
}
