{ pkgs ? import ./nix/nixpkgs.nix {} }:
let
  nl-utils = import ./nix/nl-utils.nix { inherit pkgs; };
in
rec { 
  tests = import ./tests { inherit pkgs; };
  simulator = nl-utils.mkSimulator {
    pname = "betterave";
    version = "1.0";
    entrypoint = "cpu";
    src = ./cpu;
    main = ./nix/main.c;
  };
  clock = pkgs.pkgsCross.riscv32-embedded.stdenv.mkDerivation {
    name="clock";
    src = ./assembler;
    nativeBuildInputs = [ pkgs.xxd ];
    installPhase = "mkdir -p $out; mv clock.hex $out/";
    PREFIX="riscv32-none-elf-";
  };
  clock_fast = pkgs.pkgsCross.riscv32-embedded.stdenv.mkDerivation {
    name="clock_fast";
    src = ./assembler;
    nativeBuildInputs = [ pkgs.xxd ];
    installPhase = "mkdir -p $out; mv clock_fast.hex $out/";
    PREFIX="riscv32-none-elf-";
    OUT="clock_fast";
  };
  clock_sim = pkgs.writeScript "run-clock" ''
    ${simulator}/bin/simulate-betterave ${clock}/clock.hex
    '';
  clock_sim_fast = pkgs.writeScript "run-clock-fast" ''
    ${simulator}/bin/simulate-betterave ${clock_fast}/clock_fast.hex
    '';
  buildAssets = {
    nl-sim = (import ./nix/nl-sim.nix { inherit pkgs; }).wheel;
    carottepy = (import ./nix/carottepy.nix { inherit pkgs; }).wheel;
  };
}
