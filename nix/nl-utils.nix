{ carottepy ? import ./carottepy.nix { inherit pkgs; }
, nl-sim ? import ./nl-sim.nix { inherit pkgs; }
, pkgs ? import ./nixpkgs.nix { } }: rec {
  mkSimulator = { pname, version, src, entrypoint, main ? ./main-example.c }:
    pkgs.stdenv.mkDerivation {
      inherit pname version;
      inherit src;
      buildInputs = [ carottepy nl-sim ];
      buildPhase = ''
        echo "Generating netlist"
        carotte.py -m "${entrypoint}" -y ./ -p ./ /dev/null -o netlist.nl
        echo "Generating C code"
        nl-transpile netlist.nl simulator
        echo "Copying main"
        cp ${main} ./main.c
        substituteInPlace main.c --replace betterave.h simulator.h
        echo "Compiling simulator"
        gcc -Wall -Wextra -Wno-parentheses -O4 -pthread -D_REENTRANT main.c simulator.c -o simulate-${pname}
      '';
      installPhase = ''
        mkdir -p $out/bin
        mv simulate-${pname} $out/bin/
      '';
      nl = mkNetlist { inherit pname version src entrypoint; };
    };
  mkNetlist = { pname, version, src, entrypoint }:
    pkgs.stdenv.mkDerivation {
      inherit pname version;
      inherit src;
      buildInputs = [ carottepy ];
      buildPhase = ''
        carotte.py -m "${entrypoint}" -y ./ -p ./ /dev/null -o netlist.nl
      '';
      installPhase = ''
        mv netlist.nl $out
      '';
    };

  mkTest = { pname, version, src, entrypoint, input, output, rom ? "/dev/null" }:
    let
      simulator = mkSimulator {
        inherit pname version src entrypoint;
        main = ./main-test.c;
      };
    in pkgs.runCommand "${pname}-test" {
      inherit simulator;
      nl = mkNetlist { inherit pname version src entrypoint; };
    } ''
      mkdir $out
      ${simulator}/bin/simulate-${pname} ${rom} < ${input} > $out/output.txt
      echo "Output:"
      cat $out/output.txt
      echo ""
      echo "Differences (if different):"
      diff ${output} $out/output.txt
      echo "No differences"
    '';
}
