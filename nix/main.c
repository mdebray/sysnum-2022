#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <pthread.h>

#include "betterave.h"

void set_cursor(int line, int col)
{
	printf("\033[%d;%dH", line, col);
}

bool on(uint32_t x, uint32_t bit)
{
	return ((x >> bit) & 1);
}

void init_7segm()
{
	printf("\033[2J");
	set_cursor(7, 25);
	printf(".");
	set_cursor(7, 39);
	printf(".");
	set_cursor(3, 69);
	printf(".");
	set_cursor(5, 69);
	printf(".");
	set_cursor(3, 83);
	printf(".");
	set_cursor(5, 83);
	printf(".");
}

void print_segm(uint32_t num, int bit, char *segmon, char *segmoff, uint32_t line, uint32_t col)
{
	set_cursor(line, col);
	if (on(num, bit))
		printf("%s", segmon);
	else
		printf("%s", segmoff);
}

void print_7segm(uint32_t num, uint32_t offset)
{
	print_segm(num, 0, "----", "    ", 1, offset + 2);

	print_segm(num, 1, "|", " ", 2, offset + 6);
	print_segm(num, 1, "|", " ", 3, offset + 6);

	print_segm(num, 2, "|", " ", 5, offset + 6);
	print_segm(num, 2, "|", " ", 6, offset + 6);

	print_segm(num, 3, "----", "    ", 7, offset + 2);

	print_segm(num, 4, "|", " ", 6, offset + 1);
	print_segm(num, 4, "|", " ", 5, offset + 1);

	print_segm(num, 5, "|", " ", 3, offset + 1);
	print_segm(num, 5, "|", " ", 2, offset + 1);

	print_segm(num, 6, "----", "    ", 4, offset + 2);

	set_cursor(9, 1);
	printf("\n");
}

void * print_values(void * p_data) {
	printf("ok");
	uint8_t* values = (uint8_t *) p_data;
	const uint32_t segm_display_offset[14] = { 90, 84, 76, 70, 62, 56, 46, 40, 32, 26, 18, 12, 6, 0 };
	init_7segm();
	while(1)
	{
		for (int pos = 0; pos < 14; pos++)
		{
			print_7segm(values[pos], segm_display_offset[pos]);
		}
	}
}


int main(int argc, char *argv[]) {
	Output_netlist output;
	Input_netlist input;
	Rom_netlist rom;
	uint8_t display_values[14] = {0};
	if (argc > 1) {
		if (argv[1] == "-h")
		{
			printf("help for you.\n");
			return 0;
		}
		FILE * fp = fopen(argv[1], "r");
		if(fp == NULL) {
			printf("ROM file not found");
			return 1;
		}
		fscan_rom(fp, &rom);
		rom.instruction = rom.data_rom;
	} else {
			printf("Please provide program to run (.hex file as first argument).\n");
			return 1;
	}
	pthread_t thread;
	int ret = pthread_create(&thread, NULL, print_values, display_values);
	while (1) {
		prompt_netlist_input(&input);
		simulateNetlist(&input, &output, &rom);
		if (output.output_enable && 0 < output.output_addr && output.output_addr < 8) {
			int pos = (output.output_addr -1)*2;
			display_values[pos] = (0xff) & output.output_data;
			display_values[pos+1] = (0xff) & (output.output_data >> 8);
		}
	};
	return 0;
};

