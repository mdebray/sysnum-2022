{ lib, pkgs, stdenv, setuptoolsBuildHook, buildPythonPackage, pythonPackages, fetchFromGitHub }:
buildPythonPackage rec {
  pname = "netlistSimulator";
  version = "1.0";
  doCheck = false;
  src = (import ./sources.nix).nl-sim + "/nl-transpiler";
  propagatedBuildInputs = [ pythonPackages.lark ];
  wheel = stdenv.mkDerivation {
    pname = "nl-transpiler-wheel";
    inherit src version propagatedBuildInputs;
    buildInputs = [ setuptoolsBuildHook ];
    installPhase = ''
      mkdir -p $out
      mv dist/* $out/
      '';
  };
}
