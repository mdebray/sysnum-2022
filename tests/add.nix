{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu-add";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu.add";
  input = pkgs.writeText "input-alu-add-test" ''
    35 2
    36 a1
  '';
  output = pkgs.writeText "output-alu-add-test" ''
    c=37
    c=d7
  '';
}
