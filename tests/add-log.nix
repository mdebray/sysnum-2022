{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu-add-log";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu.add_log";
  input = pkgs.writeText "input-alu-add-log-test" ''
    35 2
    36 a1
  '';
  output = pkgs.writeText "output-alu-add-log-test" ''
    c=37
    c=d7
  '';
}
