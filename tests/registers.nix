{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "registers";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.registers";
  input = pkgs.writeText "input-registers" ''
    0 0 1 3d5ab85e 1
    0 0 1 4c5f0bed 0
    1 0 0 4c5f0bed 1
    1 0 0 0 0
  '';
  output = pkgs.writeText "output-registers" ''
    rdata1=0, rdata2=0
    rdata1=0, rdata2=0
    rdata1=3d5ab85e, rdata2=0
    rdata1=3d5ab85e, rdata2=0
  '';
}
