{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu-less-log";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu.less_log";
  input = pkgs.writeText "input-alu-less-log-test" ''
    35 2
    36 a1
  '';
  output = pkgs.writeText "output-alu-less-log-test" ''
    c=0
    c=1
  '';
}
