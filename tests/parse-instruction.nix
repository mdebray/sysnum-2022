{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "parse-instruction";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.instruction_decoder.parse_instruction";
  input = pkgs.writeText "input-instruction-decoder-parse-instruction" ''
    a5f8e7bd
  '';
  output = pkgs.writeText "output-instruction-decoder-parse-instruction" ''
    b_imm=fffffa4e, funct3=6, funct7=52, i_imm=fffffa5f, j_imm=fff8ea5e, opcode=3d, rd=f, rs1=11, rs2=1f, s_imm=fffffa4f, u_imm=a5f8e000
  '';
}
