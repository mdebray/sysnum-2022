{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu-left-shift";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu.left_shift";
  input = pkgs.writeText "input-alu-left-shift-test" ''
    35 2
    36 1
  '';
  output = pkgs.writeText "output-alu-left-shift-test" ''
    c=d4
    c=6c
  '';
}
