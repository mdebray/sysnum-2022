{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "ram";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.ram";
  input = pkgs.writeText "input-ram-test" ''
    1a2e1a2e ff00ff00 2 1
    1a2e1a2e 0 2 0
    0 ffff08ff 1 1
    0 ffff08ff 1 0
    0 ffff08f5 0 1
    0 0 0 0
  '';
  output = pkgs.writeText "output-ram-test" ''
    ram=0
    ram=ff00ff00
    ram=0
    ram=8ff
    ram=ffffffff
    ram=fffffff5
  '';
}
