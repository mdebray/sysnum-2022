{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu-eq";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu.eq";
  input = pkgs.writeText "input-alu-eq-test" ''
    35 2
    36 36
  '';
  output = pkgs.writeText "output-alu-eq-test" ''
    c=0
    c=1
  '';
}
