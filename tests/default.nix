{ pkgs ? import ./nix/nixpkgs.nix { } }:
let nl-utils = import ./../nix/nl-utils.nix { inherit pkgs; };
in {
  leftSifhtTest = pkgs.callPackage ./left-shift.nix { inherit nl-utils; };
  addTest = pkgs.callPackage ./add.nix { inherit nl-utils; };
  # addLogTest = pkgs.callPackage ./add-log.nix { inherit nl-utils; };
  # lessLogTest = pkgs.callPackage ./less-log.nix { inherit nl-utils; };
  eqTest = pkgs.callPackage ./eq.nix { inherit nl-utils; };
  aluTest = pkgs.callPackage ./alu.nix { inherit nl-utils; };
  registersTest = pkgs.callPackage ./registers.nix {inherit nl-utils; };
  parseInstructionTest =
    pkgs.callPackage ./parse-instruction.nix { inherit nl-utils; };
  getopTest = pkgs.callPackage ./getop.nix { inherit nl-utils; };
  instructionDecoderTest = pkgs.callPackage ./instruction-decoder { inherit nl-utils; };
}
