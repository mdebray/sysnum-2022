{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "alu";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.alu";
  input = pkgs.writeText "input-alu-test" ''
    32 1 0
    4a 57 1
    fffffff2 3 2
    ffff1100 f0124005 2
    18 fffc 2
    fcade 12e 2
    ffff fff0 3
    ffff aaaaaa 3
    ffff0000 7fff0001 3
    f0d996f 86101370 4
    f0d996f 86101370 5
    f0d996f 86101370 6
    f0d996f 86101370 7
  '';
  output = pkgs.writeText "output-alu-test" ''
    o=33
    o=25000000
    o=1
    o=0
    o=1
    o=0
    o=0
    o=1
    o=0
    o=891d8a1f
    o=f0d
    o=8f1d9b7f
    o=6001160
  '';
}
