{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "getop";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.instruction_decoder.getop";
  input = pkgs.writeText "input-instruction-decoder-getop" ''
    33
    13
    17
    63
    6f
    67
    3
    37
    23
    0
    1
    2
    4
    5
    6
    7
    8
    9
    a
    b
    c
    d
    e
    f
    10
    11
    12
    14
    15
    16
    18
    19
    1a
    1b
    1c
    1d
    1e
    1f
    20
    21
    22
    24
    25
    26
    27
    28
    29
    2a
    2b
    2c
    2d
    2e
    2f
    30
    31
    32
    34
    35
    36
    38
    39
    3a
    3b
    3c
    3d
    3e
    3f
    40
    41
    42
    43
    44
    45
    46
    47
    48
    49
    4a
    4b
    4c
    4d
    4e
    4f
    50
    51
    52
    53
    54
    55
    56
    57
    58
    59
    5a
    5b
    5c
    5d
    5e
    5f
    60
    61
    62
    64
    65
    66
    68
    69
    6a
    6b
    6c
    6d
    6e
    70
    71
    72
    73
    74
    75
    76
    77
    78
    79
    7a
    7b
    7c
    7d
    7e
    7f
  '';
  output = pkgs.writeText "output-instruction-decoder-getop" ''
    arith=1, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=1, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=1, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=1, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=1, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=1, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=1, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=1, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=1
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
    arith=0, arith_i=0, auipc=0, branch=0, jal=0, jalr=0, load=0, lui=0, store=0
  '';
}
