{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "cpu";
  version = "1.0";
  src = ./../../cpu;
  entrypoint = "tests.instruction_decoder";
  input = ./instruction-decoder-input.txt;
  output = ./instruction-decoder-output.txt;
}
