import random


def bin2hex(x):
    return hex(int(x[::-1], 2))[2:]


def bin2int(x, b=False):
    a = int(x[::-1], 2)
    if a > 2**31 and b:
        a -= 2**32
    return a


def int2bin(x):
    if x < 0:
        x += 2**32
    a = bin(x)[-1:1:-1]
    return a + "0" * (32 - len(a))


def int2hex(x):
    return hex(x)[2:]


def sign_extend(x):
    l = 32 - len(x)
    return x + (x[-1] * l)


def upper_imm(x):
    l = 32 - len(x)
    return "0" * l + x


def j_immm(x):
    return x[12:20] + x[11] + x[1:11] + x[20]


def randbin(bits):
    r = ""
    for _ in range(bits):
        r += random.choice(("0", "1"))
    return r


# reg alu ram


def lui(pc):
    rd = randbin(5)
    imm = randbin(20)
    return (
        f"{bin2hex('1110110' + rd + imm)} 0 0 0 0",
        f"RAM_addr=80000000, RAM_data_len={bin2hex(imm[:3])}, RAM_store=0, alu1=0, alu2={bin2hex(sign_extend(imm[-12:]))}, alu_op={bin2hex(imm[:3])}, pc={int2hex(pc)}, rd={bin2hex(rd)}, rd_val={bin2hex(upper_imm(imm))}, rg_w_en=1",
    )


def auipc(pc):
    rd = randbin(5)
    imm = randbin(20)
    alu_out = randbin(32)
    return (
        f"{bin2hex('1110100' + rd + imm)} 0 0 {bin2hex(alu_out)} 0",
        f"RAM_addr=80000000, RAM_data_len={bin2hex(imm[:3])}, RAM_store=0, alu1={int2hex(pc)}, alu2={bin2hex(upper_imm(imm))}, alu_op=0, pc={int2hex(pc)}, rd={bin2hex(rd)}, rd_val={bin2hex(alu_out)}, rg_w_en=1",
    )


def jal(pc, jump):
    rd = randbin(5)
    if jump < 0:
        jump += 2**32
    imm = j_immm(int2bin(jump))
    return (
        f"{bin2hex('1111011' + rd + imm)} 0 0 0 0",
        f"RAM_addr=80000000, RAM_data_len={bin2hex(imm[:3])}, RAM_store=0, alu1=0, alu2={bin2hex(sign_extend(imm[-12:]))}, alu_op={bin2hex(imm[:3])}, pc={int2hex(pc)}, rd={bin2hex(rd)}, rd_val={int2hex(pc)}, rg_w_en=1",
    )


def jalr(pc, offset, reg_val):
    rd = randbin(5)
    rs1 = randbin(5)
    imm = int2bin(offset)[0:12]
    print(imm)
    return (
        f"{bin2hex('1110011' + rd + '000' + rs1 + imm)} {int2hex(reg_val)} 0 0 0",
        f"RAM_addr=80000000, RAM_data_len=0, RAM_store=0, alu1={int2hex(reg_val)}, alu2={bin2hex(sign_extend(imm[-12:]))}, alu_op=0, pc={int2hex(pc)}, rd={bin2hex(rd)}, rd_val={int2hex(pc)}, rg_w_en=1",
    )


def beq(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '000' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} 0 0",
        f"RAM_addr=80000000, RAM_data_len=0, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=0, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )


def bneq(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '100' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} 0 0",
        f"RAM_addr=80000000, RAM_data_len=1, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=0, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )


def blt(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '001' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} {1 if rg1 < rg2 else 0} 0",
        f"RAM_addr=80000000, RAM_data_len=4, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=2, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )


def bge(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '101' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} {1 if rg1 >= rg2 else 0} 0",
        f"RAM_addr=80000000, RAM_data_len=5, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=2, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )


def bltu(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '011' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} {1 if rg1 < rg2 else 0} 0",
        f"RAM_addr=80000000, RAM_data_len=6, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=2, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )


def bgeu(pc, rg1, rg2, offset):
    rs1 = randbin(5)
    rs2 = randbin(5)
    imm = int2bin(offset)
    imm1 = imm[11] + imm[1:5]
    imm2 = imm[5:11] + imm[12]
    return (
        f"{bin2hex('1100011' + imm1 + '111' + rs1 + rs2 + imm2)} {int2hex(rg1)} {int2hex(rg2)} {1 if rg1 >= rg2 else 0}  0",
        f"RAM_addr=80000000, RAM_data_len=7, RAM_store=0, alu1={int2hex(rg1)}, alu2={int2hex(rg2)}, alu_op=2, pc={int2hex(pc)}, rd={bin2hex(imm1)}, rd_val=0, rg_w_en=0",
    )

t = []

i = 0
while i <= 20:
    t.append(lui(i))
    i += 4
while i <= 40:
    t.append(auipc(i))
    i += 4
t.append(jal(i, -i))
i += 4
t.append(jal(0, i))
t.append(jalr(i, 0x50, 0x80000000))
i += 4
t.append(jalr(0x80000050, 0x0, i))
t.append(beq(i, 10, 10, 0x8))
i += 8
t.append(beq(i, 10, 11, 0x8))
i += 4
t.append(bneq(i, 10, 10, 0x8))
i += 4
t.append(bneq(i, 10, 11, 0x8))
i += 8

with open("instruction-decoder-input.txt", "w") as f:
    for a, _ in t:
        f.write(a + "\n")
        print(a)

with open("instruction-decoder-output.txt", "w") as f:
    for _, a in t:
        f.write(a + "\n")
        print(a)
