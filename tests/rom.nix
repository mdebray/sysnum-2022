{ nl-utils, pkgs }:
nl-utils.mkTest {
  pname = "rom";
  version = "1.0";
  src = ./../cpu;
  entrypoint = "tests.rom";
  input = pkgs.writeText "input-rom-test" ''
    0
    4
    8
    c
    10
    14
    18
    1c
  '';
  output = pkgs.writeText "output-rom-test" ''
    rom=1ba94fca
    rom=112619b
    rom=63cf6bd0
    rom=b79a3161
    rom=a924d05d
    rom=d5a75a51
    rom=805c4056
    rom=5cf240f8
  '';
  rom = ./rom_test.txt;
}
