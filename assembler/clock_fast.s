# Registres utilisés pour les compteurs x10-19
#	(ticks, secondes, minutes, heures, jours, mois, années, a%4, a%100, a%400)
# Registres utilisés pour les limites x20-29
# x1 : adresse de la date enregistrée
# x2 : adresse pour les nombres de jours du mois
# x3 : adresse pour les affichages des nombres
# x5-9 : libres

.set SPEED, 1

.globl main
main:
	la		x1, input_addr		# Arbitraire, à choisir
	la		x2, jour_mois_addr
	la		x3, output_addr
	la		x4, segm

	# On charge les jours des mois dans la RAM
	li		x6, 32
	sw		x6, (x2)	# Janvier
	sw		x6, 0x8(x2)	# Mars
	sw		x6, 0x10(x2)	# Mai
	sw		x6, 0x18(x2)	# Juillet
	sw		x6, 0x1c(x2)	# Août
	sw		x6, 0x24(x2)	# Octobre
	sw		x6, 0x2c(x2)	# Décembre

	li		x6, 31
	sw		x6, 0xc(x2)	# Avril
	sw		x6, 0x14(x2)	# Juin
	sw		x6, 0x20(x2)	# Septembre
	sw		x6, 0x28(x2)	# Novembre


	# On charge la date enregistrée
	li		x10, 0			# Ticks
	lw		x11, (x1)		# Secondes
	lw		x12, (x1)	# Minutes
	lw		x13, (x1)	# Heures
	lw		x14, (x1)	# Jour
	lw		x15, (x1)	# Mois
	lw		x17, (x1)	# Années%4
	lw		x18, (x1)	# Années%100
	lw		x19, (x1)	# Années%400
	lw		x30, (x1)	# Siècle

	# On check les annees bissextiles
	li		x6, 29
	li		x7, 30
	sw		x6, 0x4(x2)	# Février par défaut
	bne		x0, x17, end_check
	sw		x7, 0x4(x2)	# Février par bissextile
	bne		x0, x18, end_check
	sw		x6, 0x4(x2)	# Février, chaque siècle c'est pas bissextile
	bne		x0, x19, end_check
	sw		x7, 0x4(x2)	# Février


end_check:
	
	# On charge les limites pour les compteurs
	li		x20, SPEED		# Nombre de ticks par secondes
	li		x21, 60			# Nombre de secondes par minutes
	li		x22, 60
	li		x23, 24
	slli	x5, x15, 2
	add		x5, x2, x5		# Récupérer le nombre de jours du mois (stocké en RAM)
	lw		x24, -0x4(x5)
	li		x25, 13			# il y a 12 mois dans l'année mais on compte à partir de 1
	li		x27, 4			# Une année bissextile tous les 4 ans
	li		x28, 100		# Pas dannée bissextile tous les 100 ans
	li		x29, 400		# mais il se passe un truc tous les 400 ans aussi

	j		print_siecle	# On affiche quand même dès le début

main_loop:				# Boucle tant qu'une seconde ne s'est pas écoulée
	addi	x10, x10, 1
	bne		x10, x20, main_loop
secondes:
	li		x10, 0
	addi	x11, x11, 1
	bne		x11, x21, print_secondes # on s'en va si il n'y a pas besoin d'incrémenter les minutes
minutes:
	li		x11, 0			# On remet le nombre de secondes à 0
	addi	x12, x12, 1
	bne		x12, x22, print_minutes
heures:
	li		x12, 0			# On remet le nombre de minutes à 0
	addi	x13, x13, 1
	bne		x13, x23, print_heures
jours:
	li		x13, 0			# On remet le nombre d'heures à 0
	addi	x14, x14, 1
	bne		x14, x24, print_jours
mois:
	li		x14, 1			# On remet le nombre de jours à 1
	addi	x15, x15, 1
	bne		x15, x25, print_mois
ans:
	li		x15, 1
	addi	x17, x17, 1
	addi	x18, x18, 1
	addi	x19, x19, 1
	bne		x17, x27, print_ans
amod4:	# Années bissextiles
	li		x17, 0
	li		x6, 30
	sw		x6, 0x4(x2)
	bne		x18, x28, print_ans
amod100:
	addi	x30, x30, 1
	li		x18, 0
	li		x6, 29
	sw		x6, 0x4(x2)
	bne		x19, x29, print_ans
amod400:
	li		x19, 0
	li		x6, 30
	sw		x6, 0x4(x2)

# Affichage : mettre les chiffres du 7 segments dans la ram
# print_X met les chiffres de X à la position voulue
print_siecle:
	slli	x7, x30, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0x18(x3)
print_ans:
	slli	x7, x18, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0x14(x3)
print_mois:
	slli	x5, x15, 2		# on convertit en adresse
	add		x5, x2, x5		# Récupérer le nombre de jours du mois (stocké en RAM)
	lw		x24, -0x4(x5)
	slli	x7, x15, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0x10(x3)
print_jours:
	slli	x7, x14, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0xc(x3)
print_heures:
	slli	x7, x13, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0x8(x3)
print_minutes:
	slli	x7, x12, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, 0x4(x3)
print_secondes:
	slli	x7, x11, 2
	add		x7, x4, x7
	lw		x8, (x7)
	sw		x8, (x3)
	j	main_loop

.section .data
segm:
	.word 0x3f3f
	.word 0x3f06
	.word 0x3f5b
	.word 0x3f4f
	.word 0x3f66
	.word 0x3f6d
	.word 0x3f7d
	.word 0x3f07
	.word 0x3f7f
	.word 0x3f6f
	.word 0x63f
	.word 0x606
	.word 0x65b
	.word 0x64f
	.word 0x666
	.word 0x66d
	.word 0x67d
	.word 0x607
	.word 0x67f
	.word 0x66f
	.word 0x5b3f
	.word 0x5b06
	.word 0x5b5b
	.word 0x5b4f
	.word 0x5b66
	.word 0x5b6d
	.word 0x5b7d
	.word 0x5b07
	.word 0x5b7f
	.word 0x5b6f
	.word 0x4f3f
	.word 0x4f06
	.word 0x4f5b
	.word 0x4f4f
	.word 0x4f66
	.word 0x4f6d
	.word 0x4f7d
	.word 0x4f07
	.word 0x4f7f
	.word 0x4f6f
	.word 0x663f
	.word 0x6606
	.word 0x665b
	.word 0x664f
	.word 0x6666
	.word 0x666d
	.word 0x667d
	.word 0x6607
	.word 0x667f
	.word 0x666f
	.word 0x6d3f
	.word 0x6d06
	.word 0x6d5b
	.word 0x6d4f
	.word 0x6d66
	.word 0x6d6d
	.word 0x6d7d
	.word 0x6d07
	.word 0x6d7f
	.word 0x6d6f
	.word 0x7d3f
	.word 0x7d06
	.word 0x7d5b
	.word 0x7d4f
	.word 0x7d66
	.word 0x7d6d
	.word 0x7d7d
	.word 0x7d07
	.word 0x7d7f
	.word 0x7d6f
	.word 0x73f
	.word 0x706
	.word 0x75b
	.word 0x74f
	.word 0x766
	.word 0x76d
	.word 0x77d
	.word 0x707
	.word 0x77f
	.word 0x76f
	.word 0x7f3f
	.word 0x7f06
	.word 0x7f5b
	.word 0x7f4f
	.word 0x7f66
	.word 0x7f6d
	.word 0x7f7d
	.word 0x7f07
	.word 0x7f7f
	.word 0x7f6f
	.word 0x6f3f
	.word 0x6f06
	.word 0x6f5b
	.word 0x6f4f
	.word 0x6f66
	.word 0x6f6d
	.word 0x6f7d
	.word 0x6f07
	.word 0x6f7f
	.word 0x6f6f

.section .bss
	jour_mois_addr:	.space	0x400

.section io, "aw", @nobits
	input_addr:	.space	0x4
	output_addr:	.space	0x40
