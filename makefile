CAROTTEPY?=carotte.py
NLTRANSPILE?=nl-transpile
PIP?=pip

PREFIX?=riscv64-unknown-elf-
CC=$(PREFIX)gcc
OBJCOPY=$(PREFIX)objcopy
OBJDUMP=$(PREFIX)objdump
LINKER_SCRIPT=assembler/script.ld
OUT=assembler/clock
OUT_FAST=assembler/clock_fast

betterave: main.c betterave.c betterave.h makefile
	gcc -Wall -Wextra -Wno-parentheses -O4 -pthread -D_REENTRANT main.c betterave.c -o betterave

all: betterave $(OUT).hex $(OUT_FAST).hex

betterave.nl: cpu/
	$(CAROTTEPY) -m cpu -y ./cpu -p ./cpu /dev/null -o betterave.nl

betterave.c: betterave.nl
	$(NLTRANSPILE) betterave.nl betterave

betterave.h: betterave.c

main.c: ./nix/main.c
	cp ./nix/main.c ./


$(OUT).bin: $(OUT).elf
	$(OBJCOPY) -O binary $^ $@

$(OUT).elf: $(OUT).s $(LINKER_SCRIPT)
	$(CC) -march=rv32i -mabi=ilp32 -nostartfiles -nostdlib -T$(LINKER_SCRIPT) $< -o $@


$(OUT).hex: $(OUT).bin
	xxd -e -c 4 $^ | cut -c 11-18 > $@ #Oui c'est moche et hacky (-e transforme en little endian normalement mais là on utilise la réciproque qui est le fonction car c'est involutif)

$(OUT_FAST).bin: $(OUT_FAST).elf
	$(OBJCOPY) -O binary $^ $@

$(OUT_FAST).elf: $(OUT_FAST).s $(LINKER_SCRIPT)
	$(CC) -march=rv32i -mabi=ilp32 -nostartfiles -nostdlib -T$(LINKER_SCRIPT) $< -o $@


$(OUT_FAST).hex: $(OUT_FAST).bin
	xxd -e -c 4 $^ | cut -c 11-18 > $@ #Oui c'est moche et hacky (-e transforme en little endian normalement mais là on utilise la réciproque qui est le fonction car c'est involutif)


.PHONY = clean clean-all setup-env  buildInputs release

release: debray-kimbrough-douzal.tar.gz

debray-kimbrough-douzal.tar.gz:
	rm -rf debray-kimbrough-douzal
	mkdir debray-kimbrough-douzal
	cp README.md debray-kimbrough-douzal
	cp REPORT.md debray-kimbrough-douzal
	cp -r cpu/ debray-kimbrough-douzal
	cp -r nix/ debray-kimbrough-douzal
	cp -r spec/ debray-kimbrough-douzal
	cp -r assembler debray-kimbrough-douzal
	cp -r input_generator.py debray-kimbrough-douzal
	cp -r buildInputs debray-kimbrough-douzal
	cp -r makefile debray-kimbrough-douzal
	cp *.nix debray-kimbrough-douzal
	cp -r tests debray-kimbrough-douzal
	tar -zvcf debray-kimbrough-douzal.tar.gz debray-kimbrough-douzal/
	rm -rf debray-kimbrough-douzal


clean:
	rm -f $(OUT).bin $(OUT).elf
	rm -f $(OUT_FAST).bin $(OUT_FAST).elf
	rm -f betterave.nl
	rm -f betterave.h
	rm -f betterave.c
	rm -f main.c

clean-all: clean
	rm -f $(OUT)
	rm -f $(OUT_FAST)
	rm -f betterave

setup-env:
	$(PIP) install buildInputs/carottepy-1.0.2-py3-none-any.whl
	$(PIP) install buildInputs/netlistSimulator-1.0-py3-none-any.whl

buildInputs: ./nix/sources.json ./nix/nl-sim.nix ./nix/carottepy.nix
	rm -f buildInputs/*.whl
	mkdir -p buildInputs
	nix-build -A buildAssets.nl-sim
	cp result/*.whl buildInputs/
	nix-build -A buildAssets.carottepy
	cp result/*.whl buildInputs/
	chmod u+w buildInputs/*.whl
