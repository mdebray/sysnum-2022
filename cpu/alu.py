"""Providing carotte.py hdl consctructs"""
from lib_carotte import Constant, Input, allow_ribbon_logic_operations

import utils

allow_ribbon_logic_operations(True)


def _and(a, b):
    return a & b


def _or(a, b):
    return a | b


def _xor(a, b):
    return a ^ b


def eq(a, b):
    equal = ~(a ^ b)
    return utils.recursive_and(equal)


def _add_log(a, b):
    """
    Carry-lookahead adder
    (not tested, may be wrong)
    """
    assert len(a) == len(b)
    propagate = a ^ b
    generate = a & b
    carries = Constant("0")
    for i in range(1, len(a)):
        carries = carries + utils.recursive_or(
            [
                utils.recursive_and(propagate[j + 1 : i]) & generate[j]
                for j in range(i - 1)
            ]
            + [generate[i - 1]]
        )
    return carries ^ propagate


def add(a, b):
    """
    Carry-ripple adder
    """
    s = a ^ b
    rslt = s[0]
    carry = (a & b)[0]
    for i in range(1, len(a)):
        rslt += s[i] ^ carry
        carry = (a[i] & b[i]) | (carry & s[i])
    return rslt


def _less_log(a, b):
    """
    (not tested, may be wrong)
    """
    bit_less = (~a) & b  # Comparaison bit à bit
    equal = ~(a ^ b)
    to_or = [
        bit_less[i] & utils.recursive_and(equal[i + 1 :]) for i in range(len(a) - 1)
    ]
    to_or.append(bit_less[-1])
    return utils.recursive_or(to_or) + Constant("0" * 31)


def _less(a, b):
    hsb_less = Constant("0")
    hsb_equal = Constant("1")
    for i in range(len(a) - 1, -1, -1):
        a_hsb_bit = a[i]
        b_hsb_bit = b[i]
        hsb_less |= (~a_hsb_bit) & b_hsb_bit & hsb_equal
        hsb_equal &= ~(a_hsb_bit ^ b_hsb_bit)
    return hsb_less + Constant("0" * 31)


def _less_signed(a, b):
    hsb_less = Constant("0")
    hsb_equal = Constant("1")
    for i in range(len(a) - 2, -1, -1):
        a_hsb_bit = a[i]
        b_hsb_bit = b[i]
        hsb_less |= (~a_hsb_bit) & b_hsb_bit & hsb_equal
        hsb_equal &= ~(a_hsb_bit ^ b_hsb_bit)
    return ((a[-1] | ~b[-1]) & ((a[-1] & ~b[-1]) | hsb_less)) + Constant("0" * 31)


def _shift_left(a, b, n=5):
    assert (1 << n) == len(a)
    shifts = [a]
    for i in range(1, 1 << n):
        shifted = Constant("0" * i) + a[:-i]
        shifts.append(shifted)
    return utils.better_mux(b[:n], shifts)


def _shift_right(a, b, n=5):
    assert (1 << n) == len(a)
    shifts = [a]
    for i in range(1, 1 << n):
        shifted = a[i:] + Constant("0" * i)
        shifts.append(shifted)
    return utils.better_mux(b[:n], shifts)


FUNCTIONNALITIES = [
    add,
    _shift_left,
    _less_signed,
    _less,
    _xor,
    _shift_right,
    _or,
    _and,
]
OP_BIT_NUMBER = 3


def alu(a, b, op):
    """
    L'alu du CPU
    a et b sont des bus de 32 bits.
    op est un bus de 3 bits
    """
    assert OP_BIT_NUMBER == op.bus_size
    assert len(FUNCTIONNALITIES) == (1 << OP_BIT_NUMBER)
    op_results = [i(a, b) for i in FUNCTIONNALITIES]
    result = utils.better_mux(op, op_results)
    return result
