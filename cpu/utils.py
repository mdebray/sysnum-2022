from lib_carotte import (Constant, Defer, Input, Mux, Reg,
                         allow_ribbon_logic_operations)

allow_ribbon_logic_operations(True)


def reg(wdata, wenable):
    """
    Implémentation d'un registre du microprocesseur
    """
    rdata = Reg(Mux(wenable, Defer(wdata.bus_size, lambda: rdata), wdata))
    return rdata


def better_mux(choice, inputs, i=None, index=0):
    """
    Permet de faire un mux sur `1 << choice.bus_size` bus
    Le bit de poids fort est le dernier
    """
    if not isinstance(choice, list):
        choice = [choice[i] for i in range(len(choice))]
    if i == None:
        i = len(choice) - 1
    assert 0 <= i < len(choice)
    assert len(inputs) == (1 << len(choice))

    if i == 0:
        return Mux(choice[i], inputs[index], inputs[index + 1])
    left = better_mux(choice, inputs, i - 1, index)
    right = better_mux(choice, inputs, i - 1, index + (1 << i))
    return Mux(choice[i], left, right)


def recursive_op(bus, op, offset=0, size=None):
    """
    Calcule `op(a_1, a_2, a_3, ...)` où `a_i` sont les bits du bus.

    op est une opération qui prend deux bits et en ressort un seul. Elle doit
    être associative

    Le bus peut être aussi une liste de bus de même taille.
    """
    if size is None:
        size = len(bus)
    if size > 1:
        return op(
            recursive_op(bus, op, offset, size // 2),
            recursive_op(bus, op, offset + size // 2, size - size // 2),
        )
    return bus[offset]


def recursive_and(bus):
    if len(bus) == 0:  # Handle 0-length case with a constant
        return Constant("1")
    return recursive_op(bus, lambda a, b: a & b)


def recursive_or(bus):
    if len(bus) == 0:  # Handle 0-length case with a constant
        return Constant("0")
    return recursive_op(bus, lambda a, b: a | b)


def _test_mux():
    choice = Constant("01")
    l = [Input(1) for i in range(4)]
    o = better_mux(choice, l)
    o.set_as_output("out")


def _test_reg():
    wdata = Input(2)
    wenable = Input(1)
    out = reg(wdata, wenable)
    out.set_as_output("out")


def better_rev_mux(choice, v, i=None):
    if i == None:
        i = len(choice) - 1
    assert 0 <= i < len(choice)

    left = ~choice[i] & v
    right = choice[i] & v

    if i == 0:
        return [left, right]
    return better_rev_mux(choice, left, i - 1) + better_rev_mux(choice, right, i - 1)
