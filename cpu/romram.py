from lib_carotte import RAM, ROM, Constant, Defer, Input, Mux

from utils import better_mux


def ram(data_len, ram_addr, store_enable, ram_data):
    # misc
    lsb_addr = ram_addr[:2]
    sign_extension = ~data_len[2]

    # byte_mask for store
    byte_mask = better_mux(
        lsb_addr,
        [Constant("1" * 8 * i + "0" * 8 + "1" * 8 * (3 - i)) for i in range(4)],
    )
    byte_data_aligned = better_mux(
        lsb_addr,
        [
            ram_data[:8] + Constant("0" * 24),
            Constant("0" * 8) + ram_data[:8] + Constant("0" * 16),
            Constant("0" * 16) + ram_data[:8] + Constant("0" * 8),
            Constant("0" * 24) + ram_data[:8],
        ],
    )
    half_mask = Mux(
        lsb_addr[1], Constant("0" * 16 + "1" * 16), Constant("1" * 16 + "0" * 16)
    )

    half_data_aligned = Mux(
        lsb_addr[1],
        ram_data[:16] + Constant("0" * 16),
        Constant("0" * 16) + ram_data[:16],
    )
    mask = better_mux(
        data_len[0:2], [byte_mask, half_mask, Constant("0" * 32), Constant("0" * 32)]
    )
    data = better_mux(
        data_len[0:2],
        [byte_data_aligned, half_data_aligned, ram_data, Constant("0" * 32)],
    )

    data_to_store = data | (mask & Defer(32, lambda: word_value))

    data_rom = ROM(24, 32, ram_addr[2:-6])

    word_value = Mux(
        ram_addr[-1],
        data_rom,
        Mux(
            ram_addr[-2],
            RAM(
                24,
                32,
                ram_addr[2:-6],
                store_enable & ram_addr[-1] & ~ram_addr[-2],
                ram_addr[2:-6],
                data_to_store,
            ),
            Input(32, "in"),
        ),
    )

    # Handling output bus
    output_enable = ram_addr[-1] & ram_addr[-2] & store_enable
    output_enable.set_as_output("output_enable")

    ram_addr[2:-2].set_as_output("output_addr")
    ram_data.set_as_output("output_data")

    # parsing value
    bytes_values = [word_value[i * 8 : (i + 1) * 8] for i in range(4)]
    half_values = [word_value[i * 16 : (i + 1) * 16] for i in range(2)]
    byte = better_mux(lsb_addr, bytes_values)
    half = Mux(lsb_addr[1], *half_values)

    byte_rslt = byte + Mux(
        sign_extension & byte[-1], Constant("0" * 24), Constant("1" * 24)
    )
    half_rslt = half + Mux(
        sign_extension & half[-1], Constant("0" * 16), Constant("1" * 16)
    )
    return better_mux(
        data_len[0:2], [byte_rslt, half_rslt, word_value, Constant("0" * 32)]
    )


def rom(addr):
    return ROM(24, 32, addr[2:-6])
