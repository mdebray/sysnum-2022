from lib_carotte import Input
from romram import rom


def main():
    rom(Input(32, "address")).set_as_output("rom")
