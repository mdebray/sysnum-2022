from lib_carotte import Input

import alu


def main():
    """
    Unit test for sum
    """
    a = Input(32)
    b = Input(32)
    c = alu._add_log(a, b)
    c.set_as_output()
