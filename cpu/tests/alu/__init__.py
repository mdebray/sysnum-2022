from lib_carotte import *

import alu


def main():
    a = Input(32)
    b = Input(32)
    choice = Input(3)
    alu.alu(a, b, choice).set_as_output("o")
