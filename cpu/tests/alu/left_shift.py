from lib_carotte import Input

import alu


def main():
    """
    Unit test for left shift
    """
    a = Input(32)
    b = Input(32)
    c = alu._shift_left(a, b)
    c.set_as_output()
