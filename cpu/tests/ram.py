from lib_carotte import Input
from romram import ram


def main():
    ram(
        Input(3, "data_len"),
        Input(32, "address"),
        Input(1, "store_enable"),
        Input(32, "data"),
    ).set_as_output("ram")
