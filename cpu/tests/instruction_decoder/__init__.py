from lib_carotte import Input

import instruction_decoder

RD_DATA = None


def fake_rom(pc):
    pc.set_as_output("pc")
    return Input(32, "_0_instruction")


def fake_ram(data_len, addr, senable, sdata):
    data_len.set_as_output("RAM_data_len")
    addr.set_as_output("RAM_addr")
    senable.set_as_output("RAM_store")
    return Input(32, "_4_RAM")


def fake_alu(alu_in1, alu_in2, alu_op):
    alu_in1.set_as_output("alu1")
    alu_in2.set_as_output("alu2")
    alu_op.set_as_output("alu_op")
    return Input(32, "_3_alu_rslt")


def fake_registers(rs1, rs2, reg_write_enable, rd, rd_data):
    global RD_DATA
    # Pas besoin de les regarder car testés par le test "parse_instruction"
    # rs1.set_as_output("rs1")
    # rs2.set_as_output("rs2")
    reg_write_enable.set_as_output("rg_w_en")
    rd.set_as_output("rd")
    rd_data.set_as_output("rd_val")
    RD_DATA = rd_data
    return Input(32, "_1_Reg1"), Input(32, "_2_Reg2")


def main():
    instruction_decoder.ROM = fake_rom
    instruction_decoder.RAM = fake_ram
    instruction_decoder.REGISTERS = fake_registers
    instruction_decoder.ALU = fake_alu
    instruction_decoder.main()
    RD_DATA.name
