from lib_carotte import Input

import instruction_decoder


def main():
    (
        opcode,
        rd,
        funct3,
        funct7,
        rs1,
        rs2,
        u_imm,
        i_imm,
        j_imm,
        s_imm,
        b_imm,
    ) = instruction_decoder.parse_instruction(Input(32))
    opcode.set_as_output("opcode")
    rd.set_as_output("rd")
    funct3.set_as_output("funct3")
    funct7.set_as_output("funct7")
    rs1.set_as_output("rs1")
    rs2.set_as_output("rs2")
    u_imm.set_as_output("u_imm")
    i_imm.set_as_output("i_imm")
    j_imm.set_as_output("j_imm")
    s_imm.set_as_output("s_imm")
    b_imm.set_as_output("b_imm")
