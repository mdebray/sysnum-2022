from lib_carotte import Input

import instruction_decoder


def main():
    (
        lui,
        auipc,
        jal,
        jalr,
        branch,
        load,
        store,
        arith_i,
        arith,
    ) = instruction_decoder.getop(Input(7))
    lui.set_as_output("lui")
    auipc.set_as_output("auipc")
    jal.set_as_output("jal")
    jalr.set_as_output("jalr")
    branch.set_as_output("branch")
    load.set_as_output("load")
    store.set_as_output("store")
    arith_i.set_as_output("arith_i")
    arith.set_as_output("arith")
