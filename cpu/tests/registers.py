from lib_carotte import Input

from registers import registers

REG_ADDR_SIZE = 5


def main():
    we = Input(1)
    wa = Input(REG_ADDR_SIZE)
    ra1 = Input(REG_ADDR_SIZE)
    ra2 = Input(REG_ADDR_SIZE)
    wd = Input(1 << REG_ADDR_SIZE)

    rd1, rd2 = registers(ra1, ra2, we, wa, wd)
    rd1.set_as_output("rdata1")
    rd2.set_as_output("rdata2")
