from lib_carotte import (Constant, Defer, Input, Mux, Reg,
                         allow_ribbon_logic_operations)

from alu import add, alu, eq
from registers import registers
from romram import ram, rom
from utils import better_mux

allow_ribbon_logic_operations(True)


XLEN = 32

RAM = ram
ROM = rom
REGISTERS = registers
ALU = alu
NADDER = add


def main():
    # fetching
    pc = Reg(Defer(32, lambda: newpc))
    instruction = ROM(pc)

    # Parsing instruction
    (
        opcode,
        rd,
        funct3,
        funct7,
        rs1,
        rs2,
        u_imm,
        i_imm,
        j_imm,
        s_imm,
        b_imm,
    ) = parse_instruction(instruction)
    lui, auipc, jal, jalr, branch, load, store, arith_i, arith = getop(opcode)

    # RAM addr stuff
    addr = Mux(load | store, Constant("0" * 31 + "1"), Defer(32, lambda: alu_out))

    # reading and updating registers
    reg_write_enable = (lui | auipc) | (jal | jalr) | (load | arith_i | arith)
    rg1, rg2 = REGISTERS(rs1, rs2, reg_write_enable, rd, Defer(32, lambda: rd_data))

    # RAM stuff bis
    # store always takes value from rg2
    ram_read_value = RAM(funct3, addr, store, rg2)
    # What to write in registers
    # u_imm if LUI
    # ram if LOAD
    # pc if JAL[R]
    # alu_out else
    rd_data = Mux(
        lui,
        Mux(load, Mux(jal | jalr, Defer(32, lambda: alu_out), pc), ram_read_value),
        u_imm,
    )

    # ALU computing
    alu_in1 = Mux(auipc, rg1, pc)
    # store -> s_imm
    # arith|branch -> rg2
    # load|arith_i -> i_imm
    # auipc -> u_imm
    # lui|jal|jalr -> don't care
    alu_in2 = Mux(auipc, Mux(store, Mux(arith | branch, i_imm, rg2), s_imm), u_imm)
    alu_op = Mux(
        load | store | auipc,
        Mux(branch, funct3, funct3[1:] + Constant("0")),
        Constant("000"),
    )
    alu_out = ALU(alu_in1, alu_in2, alu_op)

    branch_jump = (eq(rg1, rg2) & ~funct3[1] & ~funct3[2]) | alu_out[0] & funct3[2]
    is_jump = (
        jal | jalr | (branch & (branch_jump ^ funct3[0]))
    )  # funct3[0] decides if condition must be negated
    # Dealing with address computation for jumps
    index = Mux(jalr, pc, rg1)
    offset = Mux(
        is_jump, Constant("001" + "0" * 29), Mux(jal, Mux(branch, i_imm, b_imm), j_imm)
    )
    jump_address = NADDER(offset, index)

    # computing next address (if branch, jump only if alu said yes or eq)
    newpc = jump_address


def parse_instruction(instruction):
    opcode = instruction[:7]
    rd = instruction[7:12]
    funct3 = instruction[12:15]
    rs1 = instruction[15:20]
    rs2 = instruction[20:25]
    funct7 = instruction[25:32]
    u_imm = Constant("0" * 12) + instruction[12:]
    i_imm = rs2 + funct7 + Mux(funct7[-1], Constant("0" * 20), Constant("1" * 20))
    j_imm = (
        Constant("0")
        + instruction[21:31]
        + instruction[20]
        + instruction[12:20]
        + Mux(instruction[31], Constant("0" * 12), Constant("1" * 12))
    )
    s_imm = rd + funct7 + Mux(funct7[-1], Constant("0" * 20), Constant("1" * 20))
    b_imm = (
        Constant("0")
        + s_imm[1:11]
        + rd[0]
        + Mux(funct7[-1], Constant("0" * 20), Constant("1" * 20))
    )
    return opcode, rd, funct3, funct7, rs1, rs2, u_imm, i_imm, j_imm, s_imm, b_imm


def getop(opcode):
    def op_from_str(s, opcode):
        assert len(opcode) == len(s)
        if s[0] == "0":
            op = ~opcode[0]
        else:
            op = opcode[0]
        # TODO? make and in log depth
        for i in range(1, len(s)):
            if s[i] == "0":
                op &= ~opcode[i]
            else:
                op &= opcode[i]
        return op

    opcode = [opcode[i] for i in range(len(opcode))]  # Avoid selecting multiple times
    arith = op_from_str("1100110", opcode)
    arith_i = op_from_str("1100100", opcode)
    auipc = op_from_str("1110100", opcode)
    branch = op_from_str("1100011", opcode)
    jal = op_from_str("1111011", opcode)
    jalr = op_from_str("1110011", opcode)
    load = op_from_str("1100000", opcode)
    lui = op_from_str("1110110", opcode)
    store = op_from_str("1100010", opcode)
    return lui, auipc, jal, jalr, branch, load, store, arith_i, arith
