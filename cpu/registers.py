from lib_carotte import Constant

from utils import better_mux, better_rev_mux, reg


def registers(raddr1, raddr2, wenable, waddr, wdata):
    assert raddr1.bus_size == raddr2.bus_size
    assert raddr1.bus_size == waddr.bus_size
    assert wenable.bus_size == 1

    all_we = better_rev_mux(waddr, wenable)
    regs = [Constant("0" * 32)]
    regs.extend([reg(wdata, all_we[r]) for r in range(1, 1 << len(waddr))])
    rdata1 = better_mux(raddr1, regs)
    rdata2 = better_mux(raddr2, regs)
    return (rdata1, rdata2)
